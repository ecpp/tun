list(APPEND CMAKE_PREFIX_PATH ${CMAKE_CURRENT_LIST_DIR})

# The content below is to detect compiler and set appropriate flags, in order to be able to build
# the application on target platform. It should not be changed
set(COMMON_DEBUG_DIAGNOSTIC_FLAGS
    "-WCL4 -Wformat=2 -Wformat-nonliteral -Wformat-type-confusion -Wformat-security -Wunused-parameter -Winit-self -Wcast-align -Wconversion -Wpedantic -Wnull-dereference  -Wnull-dereference -Wsign-conversion  -Wdouble-promotion"
)

# ########## ASM Options ###########
set(CMAKE_ASM_COMPILER "clang")
set(CMAKE_ASM_FLAGS "-x assembler-with-cpp" CACHE INTERNAL "Assembly flags")
set(CMAKE_ASM_FLAGS_DEBUG "-Os -g3 ${COMMON_DEBUG_DIAGNOSTIC_FLAGS}"
    CACHE INTERNAL "Additional assembly flags for DEBUG build type"
)
set(CMAKE_ASM_FLAGS_RELEASE "-Os -Wall" CACHE INTERNAL
                                              "Additional assembly flags for RELEASE build type"
)

# ########## C Options ###########
set(CMAKE_C_COMPILER "clang")
set(CMAKE_C_FLAGS "" CACHE INTERNAL "C Compilation Flags")
set(CMAKE_C_FLAGS_DEBUG "-Os -g3 ${COMMON_DEBUG_DIAGNOSTIC_FLAGS}  -Wwrite-strings -Wvla"
    CACHE INTERNAL "Additional C compilation flags for DEBUG build type"
)
set(CMAKE_C_FLAGS_RELEASE "-Os -Wall" CACHE INTERNAL
                                            "Additional C compilation flags for RELEASE build type"
)

# ########## C++ Options ###########
set(CMAKE_CXX_COMPILER "clang++")
set(CMAKE_CXX_FLAGS "" CACHE INTERNAL "C++ Compilation Flags")
set(CMAKE_CXX_FLAGS_DEBUG
    "-Os -g3 ${COMMON_DEBUG_DIAGNOSTIC_FLAGS} -Wdelete-non-virtual-dtor -Woverloaded-virtual -Wold-style-cast "
    CACHE INTERNAL "Additional C++ compilation flags for DEBUG build type"
)
set(CMAKE_CXX_FLAGS_RELEASE "-Os -Wall"
    CACHE INTERNAL "Additional C++ compilation flags for RELEASE build type"
)

set(CMAKE_LINKER "lld-link")
set(CMAKE_EXE_LINKER_FLAGS "-Wl,--gc-sections")

set(CMAKE_RANLIB "llvm-ranlib")

add_compile_options(
  -ffunction-sections -fdata-sections -fno-strict-aliasing -fno-builtin -fshort-enums -Wno-psabi
)
