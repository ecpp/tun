
#include "ecpp/private/wintun.hpp"
#include "ecpp/tun.hpp"

#include <iphlpapi.h>
#include <wintun.h>

#include <cstdlib>
#include <cstring>
#include <sstream>
#include <string>

#if __cpp_exceptions
#    include <stdexcept>
#    define ECPP_THROW(X) throw(X)
#else
#    define ECPP_THROW(X) abort()
#endif

namespace ecpp {
    struct Tun::Implementation {
        Wintun                      wintun;
        std::string                 tunName{};
        unsigned                    tunMTU;
        Wintun::adapter_handle_type adapter{};
        Wintun::session_handle_type session{};
    };


    [[nodiscard]] static std::string get_generic_name() {
        std::stringstream tmp;
        static unsigned   tunCounter{0};
        tmp << "tun" << ++tunCounter;
        return tmp.str();
    }


    Tun::Tun(std::string_view name) : impl(new Implementation) {
        if (name.empty()) {
            name = get_generic_name();
        }
        std::wstring wName(name.size(), L'#');
        std::mbstowcs(&wName[0], name.data(), name.size());

        impl->adapter = impl->wintun.open_adapter(wName.c_str());
        if (nullptr == impl->adapter) {
            impl->adapter = impl->wintun.create_adapter(wName.c_str(), L"ECPP", nullptr);
        }

        if (nullptr == impl->adapter) {
            using namespace std::literals::string_literals;
            [[maybe_unused]] std::string tmp = "Failed to create Tun interface "s + tunName;
            ECPP_THROW(std::runtime_error(tmp.c_str()));
        }
    }


    Tun::~Tun() {
        down();
        impl->wintun.close_adapter(impl->adapter);
        delete impl;
    }


    void Tun::up() {
        if (nullptr == impl->session) {
            impl->session = impl->wintun.start_session(impl->adapter);
            if (nullptr == impl->session) {
                using namespace std::literals::string_literals;
                [[maybe_unused]] std::string tmp = "Failed to bring up Tun interface "s + tunName;
                ECPP_THROW(std::runtime_error(tmp.c_str()));
            }
        }
    }


    void Tun::down() {
        if (impl->session != nullptr) {
            impl->wintun.end_session(impl->session);
            impl->session = nullptr;
        }
    }


    void Tun::set_mtu(unsigned mtu) {
    }


    std::span<std::byte> Tun::read(std::span<std::byte> buf) {
        if (impl->session) {
            auto d = impl->wintun.receive_packet(impl->session);
            if (!d.empty() && buf.size() >= d.size()) {
                std::copy(d.begin(), d.end(), buf.begin());
                impl->wintun.release_received_packet(impl->session, d);
                return buf.subspan(d.size());
            }
        }
        return {};
    }

    std::span<std::byte const> Tun::write(std::span<std::byte const> data) {
        if (impl->session) {
            auto buffer = impl->wintun.allocate_send_packet(impl->session, data.size());
            if (!buffer.empty()) {
                std::copy(data.begin(), data.end(), buffer.begin());
                impl->wintun.send_packet(impl->session, buffer);
                return data.subspan(data.size());
            }
        }
        return {};
    }

    bool Tun::set_ipv6_address(std::array<std::uint8_t, 16U> addr, unsigned prefixLength) {
        std::uint32_t address;
        std::memcpy(&address, addr.data(), addr.size());

        MIB_UNICASTIPADDRESS_ROW req;
        InitializeUnicastIpAddressEntry(&req);
        impl->wintun.get_adapter_LUID(impl->adapter, &req.InterfaceLuid);
        req.Address.Ipv6.sin6_family = AF_INET6;
        req.OnLinkPrefixLength       = prefixLength;
        req.DadState                 = IpDadStatePreferred;
        std::memcpy(&req.Address.Ipv6.sin6_addr, addr.data(), addr.size());
        auto result = CreateUnicastIpAddressEntry(&req);
        return (ERROR_SUCCESS == result) || (ERROR_OBJECT_ALREADY_EXISTS == result);
    }


    bool Tun::set_ipv4_address(std::span<std::uint8_t, 4U> addr, unsigned prefixLength) {
        MIB_UNICASTIPADDRESS_ROW req;
        InitializeUnicastIpAddressEntry(&req);
        impl->wintun.get_adapter_LUID(impl->adapter, &req.InterfaceLuid);
        req.Address.Ipv4.sin_family = AF_INET;
        req.OnLinkPrefixLength      = prefixLength;
        req.DadState                = IpDadStatePreferred;
        std::memcpy(&req.Address.Ipv4.sin_addr, addr.data(), addr.size());
        auto result = CreateUnicastIpAddressEntry(&req);
        return (ERROR_SUCCESS == result) || (ERROR_OBJECT_ALREADY_EXISTS == result);
    }
} // namespace ecpp
