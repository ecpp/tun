
#include "ecpp/tun.hpp"

#include <fcntl.h>
#include <linux/if_tun.h>
#include <linux/rtnetlink.h>
#include <net/if.h>
#include <netinet/in.h>
#include <sys/ioctl.h>
#include <unistd.h>

#include <cstring>
#include <sstream>
#include <stdexcept>
#include <string>
namespace ecpp {

    struct in6_ifreq {
        struct in6_addr ifr6_addr;
        std::uint32_t   ifr6_prefixlen;
        int             ifr6_ifindex; /* Interface index */
    };


    struct Tun::Implementation {
        int fd{-1};
        int socket{-1};
        int socket6{-1};
        int rtnetlinkSocket{-1};

        std::string name{};
    };

    [[nodiscard]] static std::string get_generic_name() noexcept {
        std::stringstream tmp;
        static unsigned   tunCounter{0};
        tmp << "tun" << ++tunCounter;
        return tmp.str();
    }


    Tun::Tun(std::string_view name) : impl(new Implementation) {
        /* Open the clonable interface */
        if (-1 == (impl->fd = ::open("/dev/net/tun", O_RDWR)))
            throw std::runtime_error("Can't open /dev/net/tun");

        if (name.empty()) {
            name = get_generic_name();
        }
        if (name.size() >= IFNAMSIZ)
            throw std::invalid_argument("Too long name");
        impl->name = name;


        struct ifreq ifr {};
        ifr.ifr_flags = IFF_TUN;

        ifr.ifr_flags |= IFF_NO_PI;
        if (-1 == ::ioctl(impl->fd, TUNSETIFF, &ifr))
            std::runtime_error("Failed to set interface name");

        /* Configure the interface */

        /* Set it persistent if needed */
        // if (isPersistent) {
        //     if (-1 == ::ioctl(impl->fd, TUNSETPERSIST, 1))
        //         throw("Failed to set persistency");
        // }

        impl->socket  = ::socket(AF_INET, SOCK_DGRAM, 0);
        impl->socket6 = ::socket(AF_INET6, SOCK_DGRAM, 0);

        // if (::ioctl(impl->socket, SIOCGIFMTU, &ifr) == -1) {
        // }
        // interfaceMTU = ifr.ifr_mtu;


        if ((impl->rtnetlinkSocket = socket(PF_NETLINK, SOCK_DGRAM, NETLINK_ROUTE)) < 0) {
            fprintf(stderr, "ERROR socket(): %s\n", strerror(errno));
        }


        struct ifinfomsg {
            unsigned char  ifi_family; /* AF_UNSPEC */
            unsigned short ifi_type;   /* Device type */
            int            ifi_index;  /* Interface index */
            unsigned int   ifi_flags;  /* Device flags  */
            unsigned int   ifi_change; /* change mask */
        }
        newIf {
            .ifi_family
            .ifi_flags = IFF_TUN | IFF_NO_PI};

    }


    Tun::~Tun() {
        down();
    }


    void Tun::up() {
        struct ifreq ifr {};

        (void)std::memcpy(ifr.ifr_name, impl->name.c_str(), sizeof(impl->name.size()));

        if (::ioctl(impl->socket, SIOCGIFFLAGS, &ifr) == -1) {
        }
        ifr.ifr_flags |= IFF_UP;

        if (::ioctl(impl->socket, SIOCSIFFLAGS, &ifr) == -1) {
        }
    }


    void Tun::down() {
        struct ifreq ifr {};

        (void)std::memcpy(ifr.ifr_name, impl->name.c_str(), sizeof(impl->name.size()));
        if (::ioctl(impl->socket, SIOCGIFFLAGS, &ifr) == -1) {
        }

        ifr.ifr_flags &= ~IFF_UP;

        if (::ioctl(impl->socket, SIOCSIFFLAGS, &ifr) == -1) {
        }
    }


    bool Tun::is_up() const {
        return (impl->socket != -1) && (impl->socket != -1);
    }


    std::span<std::byte> Tun::read(std::span<std::byte> buf) {
        int n = ::read(impl->fd, buf.data(), buf.size());
        return (n == -1) ? decltype(buf){} : buf.subspan(n);
    }

    std::span<std::byte const> Tun::write(std::span<std::byte const> data) {
        int n = ::write(impl->fd, data.data(), data.size());
        return (n == -1) ? decltype(data){} : data.subspan(n);
    }


    unsigned Tun::mtu4() const {
        struct ifreq ifr {};

        (void)std::memcpy(ifr.ifr_name, impl->name.c_str(), sizeof(impl->name.size()));
        return (0 == ::ioctl(impl->socket, SIOCGIFMTU, &ifr)) ? ifr.ifr_mtu : 0U;
    }


    bool Tun::forwarding_enabled4() const {
        return false;
    }


    bool Tun::set_mtu4(unsigned mtu) {
        struct ifreq ifr {};

        (void)std::memcpy(ifr.ifr_name, impl->name.c_str(), sizeof(impl->name.size()));
        ifr.ifr_mtu = mtu;
        return (0 == ::ioctl(impl->socket, SIOCSIFMTU, &ifr));
    }


    bool Tun::set_forwarding4(bool enabled) {
        return false;
    }


    bool Tun::set_address4(std::span<std::uint8_t, 4U> addr, unsigned prefixLength) {
        struct ifreq ifr {
            // .ifr_pref = prefixLength
        };
        (void)std::memcpy(ifr.ifr_name, impl->name.c_str(), sizeof(impl->name.size()));
        struct sockaddr_in* a = (struct sockaddr_in*)&ifr.ifr_addr;
        a->sin_family         = AF_INET;
        std::memcpy(&a->sin_addr, addr.data(), addr.size());
        return (0 == ::ioctl(impl->socket, SIOCSIFADDR, &ifr));
    }


    unsigned Tun::mtu6() const {
        struct ifreq ifr {};

        (void)std::memcpy(ifr.ifr_name, impl->name.c_str(), sizeof(impl->name.size()));
        return (0 == ::ioctl(impl->socket6, SIOCGIFMTU, &ifr)) ? ifr.ifr_mtu : 0U;
    }


    bool Tun::forwarding_enabled6() const {
        return false;
    }


    bool Tun::set_mtu6(unsigned mtu) {
        struct ifreq ifr {};

        (void)std::memcpy(ifr.ifr_name, impl->name.c_str(), sizeof(impl->name.size()));
        ifr.ifr_mtu = mtu;
        return (0 == ::ioctl(impl->socket6, SIOCSIFMTU, &ifr));
    }


    bool Tun::set_forwarding6(bool enabled) {
        return false;
    }


    bool Tun::set_address6(std::array<std::uint8_t, 16U> addr, unsigned prefixLength) {
        struct ifreq ifr {};
        (void)std::memcpy(ifr.ifr_name, impl->name.c_str(), sizeof(impl->name.size()));
        if (::ioctl(impl->socket6, SIOGIFINDEX, &ifr) != 0) {
            return false;
        }

        struct in6_ifreq ifr6 {
            .ifr6_prefixlen = prefixLength, .ifr6_ifindex = ifr.ifr_ifindex
        };

        struct sockaddr_in6* a = (struct sockaddr_in6*)&ifr.ifr_addr;
        a->sin6_family         = AF_INET6;
        std::memcpy(&ifr6.ifr6_addr, addr.data(), addr.size());

        return (0 == ::ioctl(impl->socket6, SIOCSIFADDR, &ifr6));
    }
} // namespace ecpp
