#ifndef ECPP_PRIVATE_WINTUNPP_HPP_
#define ECPP_PRIVATE_WINTUNPP_HPP_

#if __cpp_exceptions
#    include <stdexcept>
#    define ECPP_THROW(X) throw(X)
#else
#    define ECPP_THROW(X) abort()
#endif

#include <wintun.h>

#include <cstddef>
#include <span>


namespace ecpp {
    class Wintun {
      public:
        using adapter_handle_type  = WINTUN_ADAPTER_HANDLE;
        using session_handle_type  = WINTUN_SESSION_HANDLE;
        using logger_function_type = WINTUN_LOGGER_CALLBACK;

        using packet_type       = std::span<std::byte>;
        using const_packet_type = std::span<std::byte const>;

        constexpr static std::size_t packet_buffer_capacity_max{WINTUN_MAX_RING_CAPACITY};
       
        Wintun();
        ~Wintun();

        auto operator=(Wintun&&) = delete; // Disable copy/move with rule of DesDeMovA

        static inline adapter_handle_type create_adapter(const WCHAR* name, const WCHAR* tunnelType, const GUID* requestedGUID) {
            return WintunCreateAdapter(name, tunnelType, requestedGUID);
        }

        static inline adapter_handle_type open_adapter(const WCHAR* name) {
            return WintunOpenAdapter(name);
        }

        static inline void close_adapter(adapter_handle_type adapter) {
            return WintunCloseAdapter(adapter);
        }

        static inline void get_adapter_LUID(adapter_handle_type adapter, NET_LUID* luid) {
            return WintunGetAdapterLUID(adapter, luid);
        }

        static inline DWORD get_running_driver_version() {
            return WintunGetRunningDriverVersion();
        }

        static inline bool delete_driver() {
            return WintunDeleteDriver();
        }

        static inline void set_logger(logger_function_type logger) {
            return WintunSetLogger(logger);
        }


        static inline session_handle_type start_session(adapter_handle_type adapter, DWORD capacity = WINTUN_MAX_RING_CAPACITY) {
            return WintunStartSession(adapter, capacity);
        }

        static inline void end_session(session_handle_type session) {
            return WintunEndSession(session);
        }

        static inline HANDLE get_read_wait_event(session_handle_type session) {
            return WintunGetReadWaitEvent(session);
        }

        static inline packet_type receive_packet(session_handle_type session) {
            DWORD tmpSize{};
            auto  data = reinterpret_cast<std::byte*>(WintunReceivePacket(session, &tmpSize));
            return {data, static_cast<std::size_t>(tmpSize)};
        }

        static inline void release_received_packet(session_handle_type session, const_packet_type packet) {
            return WintunReleaseReceivePacket(session, reinterpret_cast<BYTE const*>(packet.data()));
        }


        static inline packet_type allocate_send_packet(session_handle_type session, std::size_t packetSize) {
            auto ptr = reinterpret_cast<std::byte*>(WintunAllocateSendPacket(session, static_cast<DWORD>(packetSize)));
            if (ptr != nullptr) {
                return {ptr, packetSize};
            }
            return {};
        }

        static inline void send_packet(session_handle_type session, packet_type packet) {
            return WintunSendPacket(session, reinterpret_cast<BYTE*>(packet.data()));
        }

      private:
        static inline WINTUN_CREATE_ADAPTER_FUNC*             WintunCreateAdapter{};
        static inline WINTUN_OPEN_ADAPTER_FUNC*               WintunOpenAdapter{};
        static inline WINTUN_CLOSE_ADAPTER_FUNC*              WintunCloseAdapter{};
        static inline WINTUN_GET_ADAPTER_LUID_FUNC*           WintunGetAdapterLUID{};
        static inline WINTUN_GET_RUNNING_DRIVER_VERSION_FUNC* WintunGetRunningDriverVersion{};
        static inline WINTUN_DELETE_DRIVER_FUNC*              WintunDeleteDriver{};
        static inline WINTUN_SET_LOGGER_FUNC*                 WintunSetLogger{};
        static inline WINTUN_START_SESSION_FUNC*              WintunStartSession{};
        static inline WINTUN_END_SESSION_FUNC*                WintunEndSession{};

        static inline WINTUN_GET_READ_WAIT_EVENT_FUNC*    WintunGetReadWaitEvent{};
        static inline WINTUN_RECEIVE_PACKET_FUNC*         WintunReceivePacket{};
        static inline WINTUN_RELEASE_RECEIVE_PACKET_FUNC* WintunReleaseReceivePacket{};
        static inline WINTUN_ALLOCATE_SEND_PACKET_FUNC*   WintunAllocateSendPacket{};
        static inline WINTUN_SEND_PACKET_FUNC*            WintunSendPacket{};

        static inline HMODULE handle{};
    };

} // namespace ecpp

#endif