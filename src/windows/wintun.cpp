#include "wintun.hpp"
#if __cpp_exceptions
#    include <stdexcept>
#    define ECPP_THROW(X) throw(X)
#else
#    define ECPP_THROW(X) abort()
#endif

namespace ecpp {
    Wintun::~Wintun() {
        if (handle != nullptr) {
            static_cast<void>(FreeLibrary(handle));
        }
    }

    Wintun::Wintun() {
        if (nullptr == handle) {
            handle = LoadLibraryExW(L"wintun.dll", NULL, LOAD_LIBRARY_SEARCH_APPLICATION_DIR | LOAD_LIBRARY_SEARCH_SYSTEM32);
            if (nullptr == handle) {
                ECPP_THROW(std::runtime_error("Failed to load wintun.dll"));
            }

            auto loadSymbol = [h = handle](char const* name) { return GetProcAddress(h, name); };

#ifdef __GNUC__
#    pragma GCC diagnostic push
#    pragma GCC diagnostic ignored "-Wcast-function-type" // Surpress the warning on reinterpret_cast from FARPROC_ - this is the intended use of WINAPI
#endif
            WintunCreateAdapter           = reinterpret_cast<decltype(WintunCreateAdapter)>(loadSymbol("WintunCreateAdapter"));
            WintunCloseAdapter            = reinterpret_cast<decltype(WintunCloseAdapter)>(loadSymbol("WintunCloseAdapter"));
            WintunOpenAdapter             = reinterpret_cast<decltype(WintunOpenAdapter)>(loadSymbol("WintunOpenAdapter"));
            WintunGetAdapterLUID          = reinterpret_cast<decltype(WintunGetAdapterLUID)>(loadSymbol("WintunGetAdapterLUID"));
            WintunGetRunningDriverVersion = reinterpret_cast<decltype(WintunGetRunningDriverVersion)>(loadSymbol("WintunGetRunningDriverVersion"));
            WintunDeleteDriver            = reinterpret_cast<decltype(WintunDeleteDriver)>(loadSymbol("WintunDeleteDriver"));
            WintunSetLogger               = reinterpret_cast<decltype(WintunSetLogger)>(loadSymbol("WintunSetLogger"));
            WintunStartSession            = reinterpret_cast<decltype(WintunStartSession)>(loadSymbol("WintunStartSession"));
            WintunEndSession              = reinterpret_cast<decltype(WintunEndSession)>(loadSymbol("WintunEndSession"));
            WintunGetReadWaitEvent        = reinterpret_cast<decltype(WintunGetReadWaitEvent)>(loadSymbol("WintunGetReadWaitEvent"));
            WintunReceivePacket           = reinterpret_cast<decltype(WintunReceivePacket)>(loadSymbol("WintunReceivePacket"));
            WintunReleaseReceivePacket    = reinterpret_cast<decltype(WintunReleaseReceivePacket)>(loadSymbol("WintunReleaseReceivePacket"));
            WintunAllocateSendPacket      = reinterpret_cast<decltype(WintunAllocateSendPacket)>(loadSymbol("WintunAllocateSendPacket"));
            WintunSendPacket              = reinterpret_cast<decltype(WintunSendPacket)>(loadSymbol("WintunSendPacket"));

#ifdef __GNUC__
#    pragma GCC diagnostic pop
#endif
            if ((nullptr == WintunCreateAdapter) || (nullptr == WintunCloseAdapter) || (nullptr == WintunOpenAdapter) || (nullptr == WintunGetAdapterLUID) || (nullptr == WintunGetRunningDriverVersion) ||
                (nullptr == WintunDeleteDriver) || (nullptr == WintunSetLogger) || (nullptr == WintunStartSession) || (nullptr == WintunEndSession) || (nullptr == WintunGetReadWaitEvent) ||
                (nullptr == WintunReceivePacket) || (nullptr == WintunReleaseReceivePacket) || (nullptr == WintunAllocateSendPacket) || (nullptr == WintunSendPacket)) {
                static_cast<void>(FreeLibrary(handle));
                ECPP_THROW(std::runtime_error("Failed to load wintun.dll"));
            }
        }
    }



} // namespace ecpp