#include "ecpp/tun.hpp"
#include "wintun.hpp"

#include <iphlpapi.h>
#include <netioapi.h>

#include <cstdlib>
#include <cstring>
#include <sstream>
#include <string>

#if __cpp_exceptions
#    include <stdexcept>
#    define ECPP_THROW(X) throw(X)
#else
#    define ECPP_THROW(X) abort()
#endif

namespace ecpp {
    using namespace std::literals::string_literals;
    static Wintun wintun;
    struct Tun::Implementation {
        Wintun::adapter_handle_type adapter{};
        Wintun::session_handle_type session{};
        std::string                 name{};
    };


    [[nodiscard]] static std::string get_generic_name() noexcept {
        std::stringstream tmp;
        static unsigned   tunCounter{0};
        tmp << "tun" << ++tunCounter;
        return tmp.str();
    }

    Tun::Tun(std::string_view name) : impl(std::make_unique<Implementation>()) {
        if (name.empty()) {
            name = get_generic_name();
        }
        impl->name = name;

        std::wstring wName(impl->name.size(), L'#');
        std::mbstowcs(&wName[0], impl->name.data(), impl->name.size());

        impl->adapter = wintun.open_adapter(wName.c_str());
        if (nullptr == impl->adapter) {
            impl->adapter = wintun.create_adapter(wName.c_str(), L"ECPP", nullptr);
        }

        if (nullptr == impl->adapter) {
            [[maybe_unused]] std::string tmp = "Failed to create Tun interface "s + impl->name;
            ECPP_THROW(std::runtime_error(tmp.c_str()));
        }
    }

    Tun::~Tun() {
        down();
        wintun.close_adapter(impl->adapter);
    }

    void Tun::up() {
        if (nullptr == impl->session) {
            impl->session = wintun.start_session(impl->adapter);
            if (nullptr == impl->session) {
                [[maybe_unused]] std::string tmp = "Failed to bring up Tun interface "s + impl->name;
                ECPP_THROW(std::runtime_error(tmp.c_str()));
            }
        }
    }

    void Tun::down() {
        if (impl->session != nullptr) {
            wintun.end_session(impl->session);
            impl->session = nullptr;
        }
    }

    bool Tun::is_up() const {
        return impl->session != nullptr;
    }

    std::span<std::byte> Tun::read(std::span<std::byte> buf) {
        if (impl->session) {
            auto d = wintun.receive_packet(impl->session);
            if (!d.empty() && buf.size() >= d.size()) {
                std::copy(d.begin(), d.end(), buf.begin());
                wintun.release_received_packet(impl->session, d);
                return buf.subspan(d.size());
            }
        }
        return {};
    }

    std::span<std::byte const> Tun::write(std::span<std::byte const> data) {
        if (impl->session) {
            auto buffer = wintun.allocate_send_packet(impl->session, data.size());
            if (!buffer.empty()) {
                std::copy(data.begin(), data.end(), buffer.begin());
                wintun.send_packet(impl->session, buffer);
                return data.subspan(data.size());
            }
        }
        return {};
    }

    unsigned Tun::mtu4() const {
        MIB_IPINTERFACE_ROW req;
        InitializeIpInterfaceEntry(&req);
        wintun.get_adapter_LUID(impl->adapter, &req.InterfaceLuid);
        req.Family = AF_INET;
        if (NO_ERROR != GetIpInterfaceEntry(&req)) {
            [[maybe_unused]] std::string tmp = "Failed to get MTU from Tun interface "s + impl->name;
            ECPP_THROW(std::runtime_error(tmp.c_str()));
        }
        return req.NlMtu;
    }

    bool Tun::forwarding_enabled4() const {
        MIB_IPINTERFACE_ROW req;
        InitializeIpInterfaceEntry(&req);
        wintun.get_adapter_LUID(impl->adapter, &req.InterfaceLuid);
        req.Family = AF_INET;
        if (NO_ERROR != GetIpInterfaceEntry(&req)) {
            [[maybe_unused]] std::string tmp = "Failed to get Forwarding setting from Tun interface "s + impl->name;
            ECPP_THROW(std::runtime_error(tmp.c_str()));
        }
        return req.ForwardingEnabled;
    }

    bool Tun::set_mtu4(unsigned mtu) {
        MIB_IPINTERFACE_ROW req;
        InitializeIpInterfaceEntry(&req);
        wintun.get_adapter_LUID(impl->adapter, &req.InterfaceLuid);
        req.Family = AF_INET;
        req.NlMtu  = mtu;

        return (NO_ERROR == SetIpInterfaceEntry(&req));
    }

    bool Tun::set_forwarding4(bool enabled) {
        MIB_IPINTERFACE_ROW req;
        InitializeIpInterfaceEntry(&req);
        wintun.get_adapter_LUID(impl->adapter, &req.InterfaceLuid);
        req.Family            = AF_INET;
        req.ForwardingEnabled = enabled;
        return (NO_ERROR == SetIpInterfaceEntry(&req));
    }

    bool Tun::set_address4(std::span<std::uint8_t, 4U> addr, unsigned prefixLength) {
        MIB_UNICASTIPADDRESS_ROW req;
        InitializeUnicastIpAddressEntry(&req);
        wintun.get_adapter_LUID(impl->adapter, &req.InterfaceLuid);
        req.Address.Ipv4.sin_family = AF_INET;
        req.OnLinkPrefixLength      = static_cast<std::uint8_t>(prefixLength);
        req.DadState                = IpDadStatePreferred;
        std::memcpy(&req.Address.Ipv4.sin_addr, addr.data(), addr.size());
        auto result = CreateUnicastIpAddressEntry(&req);
        return (ERROR_SUCCESS == result) || (ERROR_OBJECT_ALREADY_EXISTS == result);
    }

    unsigned Tun::mtu6() const {
        MIB_IPINTERFACE_ROW req;
        InitializeIpInterfaceEntry(&req);
        wintun.get_adapter_LUID(impl->adapter, &req.InterfaceLuid);
        req.Family = AF_INET6;
        if (NO_ERROR != GetIpInterfaceEntry(&req)) {
            [[maybe_unused]] std::string tmp = "Failed to get MTU from Tun interface "s + impl->name;
            ECPP_THROW(std::runtime_error(tmp.c_str()));
        }
        return req.NlMtu;
    }

    bool Tun::forwarding_enabled6() const {
        MIB_IPINTERFACE_ROW req;
        InitializeIpInterfaceEntry(&req);
        wintun.get_adapter_LUID(impl->adapter, &req.InterfaceLuid);
        req.Family = AF_INET6;
        if (NO_ERROR != GetIpInterfaceEntry(&req)) {
            [[maybe_unused]] std::string tmp = "Failed to get Forwarding setting from Tun interface "s + impl->name;
            ECPP_THROW(std::runtime_error(tmp.c_str()));
        }
        return req.ForwardingEnabled;
    }

    bool Tun::set_mtu6(unsigned mtu) {
        MIB_IPINTERFACE_ROW req;
        InitializeIpInterfaceEntry(&req);
        wintun.get_adapter_LUID(impl->adapter, &req.InterfaceLuid);
        req.Family = AF_INET6;
        req.NlMtu  = mtu;

        return (NO_ERROR == SetIpInterfaceEntry(&req));
    }

    bool Tun::set_forwarding6(bool enabled) {
        MIB_IPINTERFACE_ROW req;
        InitializeIpInterfaceEntry(&req);
        wintun.get_adapter_LUID(impl->adapter, &req.InterfaceLuid);
        req.Family            = AF_INET6;
        req.ForwardingEnabled = enabled;
        return (NO_ERROR == SetIpInterfaceEntry(&req));
    }

    bool Tun::set_address6(std::array<std::uint8_t, 16U> addr, unsigned prefixLength) {
        MIB_UNICASTIPADDRESS_ROW req;
        InitializeUnicastIpAddressEntry(&req);
        wintun.get_adapter_LUID(impl->adapter, &req.InterfaceLuid);
        req.Address.Ipv6.sin6_family = AF_INET6;
        req.OnLinkPrefixLength       = static_cast<std::uint8_t>(prefixLength);
        req.DadState                 = IpDadStatePreferred;
        std::memcpy(&req.Address.Ipv6.sin6_addr, addr.data(), addr.size());
        auto result = CreateUnicastIpAddressEntry(&req);
        return (ERROR_SUCCESS == result) || (ERROR_OBJECT_ALREADY_EXISTS == result);
    }

} // namespace ecpp
