#ifndef ECPP_TUN_HPP_
#define ECPP_TUN_HPP_

#include <cstddef>
#include <cstdint>
#include <memory>
#include <span>
#include <string>
#include <string_view>
namespace ecpp {

    class Tun {
      public:
        struct Implementation;

        Tun(std::string_view name = {});
        ~Tun();
        Tun& operator=(Tun&&) = delete;

        void up();
        void down();
        bool is_up() const;

        std::span<std::byte>       read(std::span<std::byte> buf);
        std::span<std::byte const> write(std::span<std::byte const> buf);

        [[nodiscard]] std::string_view name() const;

        [[nodiscard]] unsigned mtu6() const;
        bool                   set_mtu6(unsigned mtu);
        bool                   set_address6(std::array<std::uint8_t, 16U> addr, unsigned prefixLength);
        bool                   forwarding_enabled6() const;
        bool                   set_forwarding6(bool enabled);


        [[nodiscard]] unsigned mtu4() const;
        bool                   set_mtu4(unsigned mtu);
        bool                   set_address4(std::span<std::uint8_t, 4U> addr, unsigned prefixLength);
        bool                   forwarding_enabled4() const;
        bool                   set_forwarding4(bool enabled);

      protected:
        std::unique_ptr<Implementation> impl;
    };


} // namespace ecpp

#endif