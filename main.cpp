#include "ecpp/tun.hpp"

#include <iostream>
#include <thread>
using namespace std::literals::chrono_literals;
int main() {
    ecpp::Tun tun;
    ecpp::Tun tun1;

    std::cout << "interfaces set up\n";
    std::this_thread::sleep_for(5s);

    std::cout << "turning interface on\n";
    tun.up();
    tun.set_mtu4(2000);
    std::this_thread::sleep_for(10s);
    std::cout << "turning interface on\n";
    tun.down();


    std::cout << "bye!" << std::endl;
    return 0;
}